# daverona/compose/redmine

## Prerequisites

* `redmine.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/compose/traefik) out

## Quick Start

```bash
cp .env.example .env
# edit .env
cp storage/redmine/conf/configuration.yml.example storage/redmine/conf/configuration.yml
# edit storage/redmine/conf/configuration.yml
docker-compose up --detach
```

Wait until redmine is up and visit [http://redmine.example/admin](http://redmine.example/admin).
The default username and password is `admin` and `admin` respectively.

## References

* Redmine guide: [https://www.redmine.org/projects/redmine/wiki/Guide](https://www.redmine.org/projects/redmine/wiki/Guide)
* Email Configuration: [https://www.redmine.org/projects/redmine/wiki/emailconfiguration](https://www.redmine.org/projects/redmine/wiki/emailconfiguration)
* Redmine repository: [https://github.com/redmine/redmine](https://github.com/redmine/redmine)
* Redmine registry: [https://hub.docker.com/\_/redmine](https://hub.docker.com/_/redmine)
